#!/usr/bin/env python3

import sys
import json
from datetime import datetime
import time

#### CONFIG

DIR = 'data/'
SOURCE = 'Kickstarter_2019-02-14T03_20_04_734Z.json'
DATE_FORMAT = '%Y-%m-%d'
INTERVAL = 0.05 # seconds between updates

#### INPUT

SOURCE = sys.argv[1]

#### INPUT-DEPENDENT CONFIG

TARGET = SOURCE+'.csv'

#### FIELDS

fields_auto = [
	'state',
	'backers_count', 
	'country', 'currency', 'goal', 'pledged',
	'static_usd_rate', 'usd_pledged', 'converted_pledged_amount', 'fx_rate', 'current_currency',
]
fields_date = [
	'deadline', 'created_at', 'launched_at',
]
fields_manual = [
	'category',
	'name', 'url',
]

#### MAIN

dest = open( TARGET, 'w' )

buf = ''
for field in fields_auto:
	buf = buf + field + ','
for field in fields_date:
	buf = buf + field + ','
for field in fields_manual:
	buf = buf + field + ','
buf = buf.rstrip(',') + '\n'
dest.write( buf )

count = 0
last_output = None
with open( SOURCE, 'r' ) as file_whole:
	for line in file_whole:
		count = count + 1
		
		tm = time.time()
		if (last_output==None) or (last_output+INTERVAL < tm) :
			sys.stdout.write( '\rProject count: '+str(count)+'…' )
			last_output = tm
		
		parsed = json.loads( line )
		buf = ''
		
		## fields_auto
		
		for field in fields_auto:
			buf = buf + str(parsed['data'][field])
			buf = buf + ','
		
		## fields_date
		
		for field in fields_date:
			dt = int(parsed['data'][field])
			buf = buf + datetime.utcfromtimestamp(dt).strftime(DATE_FORMAT)
			buf = buf + ','
		
		## fields_manual
		
		buf = buf + str(parsed['data']['category']['name']).replace(',','_')
		buf = buf + ','
		buf = buf + str(parsed['data']['name']).replace(',','_')
		buf = buf + ','
		buf = buf + str(parsed['data']['urls']['web']['project']).replace(',','%2C')
		
		#
		
		buf = buf + '\n'
		dest.write( buf )

print( '\rTotal projects: '+str(count) )

dest.close()
